Pod::Spec.new do |s|
  s.name         = "CSLSwift"
  s.version      = "0.0.4"
  s.summary      = "Reusable logic of service layer, managing cache and server requests"

  s.homepage     = "https://bitbucket.org/surfstudio/cslswift"
  s.license      = "MIT"
  s.author       = { "Dmitry Trofimov" => "dmitry.trofimov@surfstudio.ru" }

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"

  s.source       = { :git => "https://bitbucket.org/surfstudio/cslswift", :tag => "#{s.version}" }
  s.source_files = "CSLSwift/**/*.{swift}"
  s.dependency 'SurfObjcUtils/Core', '~> 0.1.0'
end

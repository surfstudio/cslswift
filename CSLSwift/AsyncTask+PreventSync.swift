//
//  AsyncTask+PreventSync.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 07/03/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

extension AsyncTask {
    func protectFromSyncCompletion() -> AsyncTask {
        var theSameCycle = true
        DispatchQueue.main.async {
            theSameCycle = false
        }
        let protected = AsyncTask { result in
            if theSameCycle {
                DispatchQueue.main.async {
                    self.complete(withResult: result)
                }
            } else {
                self.complete(withResult: result)
            }
        }
        self.onComplete { (result) in
            protected.complete(withResult: result)
        }
        return protected
    }
}

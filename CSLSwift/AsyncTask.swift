//
//  AsyncTask.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 07/03/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public enum AsyncTaskState {
    case active
    case completing
    case completed
}

public protocol AnyAsyncTask {
    var state: AsyncTaskState { get }
    func onCompleteIgnoringResult(afterCompletion: Bool, handler: @escaping () -> ())
}

public class AsyncTask<ResultType>: AnyAsyncTask {
    private var mutex = NSObject()
    
    private var completion: ((ResultType) -> ())?
    public private(set) var state: AsyncTaskState = .active
    public private(set) var result: ResultType?
    
    public init(completion: @escaping (ResultType) -> ()) {
        self.completion = completion
    }
    
    public init() {
        self.completion = { _ in }
    }
    
    public init(result: ResultType) {
        self.completion = { _ in }
        self.complete(withResult: result)
    }
    
    public func onComplete(afterCompletion: Bool = false, handler: @escaping (ResultType) -> ()) {
        var callHandlerNow = false
        mutex.synchronized {
            if state == .active {
                let oldCompletion = completion!
                completion = { result in
                    if (afterCompletion) {
                        oldCompletion(result)
                        handler(result)
                    } else {
                        handler(result)
                        oldCompletion(result)
                    }
                }
            } else {
                callHandlerNow = true
            }
        }
        if callHandlerNow {
            handler(result!)
        }
    }
    
    public func onCompleteIgnoringResult(afterCompletion: Bool = false, handler: @escaping () -> ()) {
        onComplete(afterCompletion: afterCompletion) { (_) in
            handler()
        }
    }
    
    @discardableResult public func complete(block: () -> (ResultType)) -> Bool {
        let alreadyCompleted: Bool = mutex.synchronized {
            if state == .active {
                state = .completing
                return false
            }
            return true
        }
        guard !alreadyCompleted else { return false }
        result = block()
        completion!(result!)
        mutex.synchronized {
            completion = nil
            state = .completed
        }
        return true
    }
    
    @discardableResult public func complete(withResult result: ResultType) -> Bool {
        return complete {
            return result
        }
    }
}

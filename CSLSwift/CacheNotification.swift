//
//  CacheNotification.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 27/02/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation
import SurfObjcUtils

public class CacheNotification {
    public let aspects: [String]
    public let request: AnyRequest?
    public let cachedResponse: AnyResponse?
    public let serverResponse: AnyResponse?
    public let response: AnyResponse?
    public let userInfo: [AnyHashable: Any]?
    
    private static let notificationName = Notification.Name("CSLCacheUpdatedNotification")
    private static let structureKey = "structure"
    
    public init(aspects: [String], request: AnyRequest? = nil, cachedResponse: AnyResponse? = nil, serverResponse: AnyResponse? = nil, response: AnyResponse? = nil, userInfo: [AnyHashable: Any]? = nil) {
        self.aspects = aspects
        self.request = request
        self.cachedResponse = cachedResponse
        self.serverResponse = serverResponse
        self.response = response
        self.userInfo = userInfo
    }
    
    public func post() {
        guard aspects.count > 0 else { return }
        let info = [CacheNotification.structureKey: self]
        NotificationCenter.default.post(name: CacheNotification.notificationName, object: nil, userInfo: info)
    }
    
    public static func subscribe(aspects: [String], holder: AnyObject, handler: @escaping (CacheNotification) -> ()) {
        let observer = NotificationCenter.default.addObserver(forName: CacheNotification.notificationName, object: nil, queue: nil) { (notif) in
            guard let info = notif.userInfo,
                let structure = info[CacheNotification.structureKey] as? CacheNotification else { return }
            guard aspects.contains(where: { structure.aspects.contains($0) }) else { return }
            handler(structure)
        }
        holder.srf_addDeallocationHandler { 
            NotificationCenter.default.removeObserver(observer)
        }
    }
}

//
//  Mutex.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 07/03/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

extension NSObject {
    func synchronized<T>(block: () -> (T)) -> T {
        objc_sync_enter(self)
        let res = block()
        objc_sync_exit(self)
        return res
    }
}

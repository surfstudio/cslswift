//
//  PrevStateTracker.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 17/04/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public class PrevStateTracker<State> {
    public var state: State
    public private(set) var prevState: State
    public init(state: State) {
        self.state = state
        self.prevState = state
    }
    
    public func didUpdate() {
        prevState = state
    }
}

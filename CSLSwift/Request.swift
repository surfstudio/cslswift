//
//  Request.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 27/02/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public enum CachePolicy {
    case serverOnly
    case cacheOnly
    case firstCacheIfFailedThenServer
    case firstCacheThenRefreshFromServer
    case firstServerIfFailedThenCache
    case firstServerWithTimeoutThenCache(timeout: TimeInterval)
}

public protocol AnyRequest {
    var cachePolicy: CachePolicy { get }
    var unsafeResponse: AnyResponse? { get }
}

open class Request <ResponseType: AnyResponse, CachedResponseType: AnyResponse>: AnyRequest {
    public var cachePolicy: CachePolicy = .serverOnly
    public var shouldAvoidCompletionOnTheSameEventLoop = false
    public var shouldReturnCacheForNotModifiedServerFailure = true
    public var shouldReuseCachedResponse = true
    public var shouldSetNotModifiedIfNoSuccessfulServerResponse = true
    public func shouldSetNotModifiedWhenCachedResponseChanged(from: CachedResponseType, to: CachedResponseType) -> Bool {
        return false
    }
    public func shouldSetNotModifiedWhenResponseChanged(from: ResponseType, to: ResponseType) -> Bool {
        return false
    }
    
    public private(set) var controller: RequestController<ResponseType, CachedResponseType>
    public private(set) var task: AsyncTask<ResponseType>?
    public var response: ResponseType? {
        return task?.result
    }
    public var unsafeResponse: AnyResponse? {
        return response
    }

    public convenience init() {
        self.init(controller: RequestController<ResponseType, CachedResponseType>())
    }

    public init(controller: RequestController<ResponseType, CachedResponseType>) {
        self.controller = controller
        ServiceManager.shared.delegate?.requestWasCreated(request: self)
    }
    
    public func perform(withCompletion completion: @escaping (ResponseType) -> ()) {
        var shouldStart = false
        let task: AsyncTask<ResponseType> = controller.mutex.synchronized {
            if let task = self.task { return task }
            shouldStart = true
            let task = AsyncTask<ResponseType>()
            self.task = task
            return task
        }
        task.onComplete(handler: completion)
        guard shouldStart else { return }
        
        controller.remember(currentRequest: self, during: task)
        
        ServiceManager.shared.delegate?.requestWillStart(request: self)
        self.perform(withTask: task)
        task.onComplete { response in
            self.controller.responseHolder.remember(response: response)
            ServiceManager.shared.delegate?.requestDidFinish(request: self)
        }
    }
    
    func perform(withTask task: AsyncTask<ResponseType>) {
        let task = shouldAvoidCompletionOnTheSameEventLoop ? task.protectFromSyncCompletion() : task
        
        let requestCacheOrServerTask = AsyncTask<()>()
        self.requestCacheOrServer(withTask: requestCacheOrServerTask)
        requestCacheOrServerTask.onComplete {
            let shouldRequestCacheToHandleNotModifiedFailure: Bool = {
                if self.shouldReturnCacheForNotModifiedServerFailure &&
                    self.requestCacheTask == nil,
                    let requestServerTask = self.requestServerTask,
                    let serverResponse = requestServerTask.result,
                    !serverResponse.unsafeResult.successful &&
                        serverResponse.notModified {
                    return true
                }
                return false
            }()
            
            let additionalRequestCacheTask = AsyncTask<()>()
            if shouldRequestCacheToHandleNotModifiedFailure {
                self.requestCacheIfNotAlready { _ in
                    additionalRequestCacheTask.complete(withResult: ())
                }
            } else {
                additionalRequestCacheTask.complete(withResult: ())
            }
            additionalRequestCacheTask.onComplete {
                self.composeAndPostprocessAndCheckModification(withTask: task)
            }
        }
    }
    
    func composeAndCheckModification() -> CachedResponseType {
        let oldCachedResponse = self.controller.cachedResponseHolder.lastSuccessfulResponse
        let serverResponse = self.requestServerTask?.result
        let cacheResponse = self.requestCacheTask?.result
        var cachedResponse: CachedResponseType = {
            if let serverResponse = serverResponse, let cacheResponse = cacheResponse {
                return self.compose(serverResponse: serverResponse, cacheResponse: cacheResponse)
            } else {
                return serverResponse ?? cacheResponse!
            }
        }()
        cachedResponse.notModified = {
            if cachedResponse.notModified { return true }
            if self.shouldSetNotModifiedIfNoSuccessfulServerResponse {
                guard let serverResponse = serverResponse, serverResponse.unsafeResult.successful else {
                    return true
                }
            }
            if let oldCachedResponse = oldCachedResponse,
                self.shouldSetNotModifiedWhenCachedResponseChanged(from: oldCachedResponse, to: cachedResponse) {
                return true
            }
            return false
        }()
        return cachedResponse
    }
    
    func composeAndPostprocessAndCheckModification(withTask task: AsyncTask<ResponseType>) {
        let cachedResponse = composeAndCheckModification()
        self.controller.cachedResponseHolder.remember(response: cachedResponse)
        
        let oldResponse = self.controller.responseHolder.lastSuccessfulResponse
        let postprocessTask = AsyncTask<ResponseType>()
        if let oldResponse = oldResponse, cachedResponse.notModified {
            var response = oldResponse
            response.notModified = true
            postprocessTask.complete(withResult: response)
        } else {
            self.postprocess(cachedResponse: cachedResponse, task: postprocessTask)
        }
        postprocessTask.onComplete { originalResponse in
            var response = originalResponse
            if !response.notModified,
                let oldResponse = oldResponse,
                self.shouldSetNotModifiedWhenResponseChanged(from: oldResponse, to: response) {
                response.notModified = true
            }
            task.complete(withResult: response)
        }
    }
    
    /// Override this if you want to include both server-specific details and cache response
    open func compose(serverResponse: CachedResponseType, cacheResponse: CachedResponseType) -> CachedResponseType {
        if serverResponse.unsafeResult.successful { return serverResponse }
        return cacheResponse.unsafeResult.successful ? cacheResponse : serverResponse
    }
    
    func requestCacheOrServer(withTask task: AsyncTask<()>) {
        switch cachePolicy {
        case .serverOnly:
            self.requestServerIfNotAlready { response in
                task.complete(withResult: ())
            }
        case .cacheOnly:
            self.requestCacheIfNotAlready { response in
                task.complete(withResult: ())
            }
        case .firstCacheIfFailedThenServer:
            self.requestCacheIfNotAlready { response in
                if response.unsafeResult.successful {
                    task.complete(withResult: ())
                } else {
                    self.requestServerIfNotAlready { response in
                        task.complete(withResult: ())
                    }
                }
            }
        case .firstCacheThenRefreshFromServer:
            self.requestCacheIfNotAlready { response in
                if response.unsafeResult.successful {
                    task.complete(withResult: ())
                }
                self.requestServerIfNotAlready { response in
                    if !task.complete(withResult: ()) {
                        self.processUnhandledServerResponse()
                    }
                }
            }
        case .firstServerIfFailedThenCache:
            self.requestServerIfNotAlready { response in
                if response.unsafeResult.successful {
                    task.complete(withResult: ())
                } else {
                    self.requestCacheIfNotAlready { cacheResponse in
                        task.complete(withResult: ())
                    }
                }
            }
        case let .firstServerWithTimeoutThenCache(timeout: timeout):
            self.requestServerIfNotAlready { response in
                if response.unsafeResult.successful {
                    if !task.complete(withResult: ()) {
                        self.processUnhandledServerResponse()
                    }
                } else {
                    self.requestCacheIfNotAlready { cacheResponse in
                        if !task.complete(withResult: ()) {
                            self.processUnhandledServerResponse()
                        }
                    }
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
                guard self.requestServerTask!.state == .active else { return }
                self.requestCacheIfNotAlready { _ in
                    task.complete(withResult: ())
                }
            }
        }
    }
    
    func processUnhandledServerResponse() {
        let cachedResponse = composeAndCheckModification()
        controller.cachedResponseHolder.remember(response: cachedResponse)
        controller.unhandledServerResponseDidHappen()
        ServiceManager.shared.delegate?.requestDidReceiveUnhandledResponse(request: self, cachedResponse: cachedResponse)
    }
    
    public func cancel(withResponse response: ResponseType) {
        task?.complete(withResult: response)
    }
    
    private func start<ResultType>(existingTask: inout AsyncTask<ResultType>?, implementation: (AsyncTask<ResultType>) -> ()) {
        var shouldStart = false
        let mutex = controller.mutex
        let task: AsyncTask<ResultType> = mutex.synchronized {
            if let task = existingTask { return task }
            let task = AsyncTask<ResultType>()
            shouldStart = true
            existingTask = task
            return task
        }
        if shouldStart {
            implementation(task)
        }
    }
    
    public private(set) var requestCacheTask: AsyncTask<CachedResponseType>?
    open func requestCache(withTask task: AsyncTask<CachedResponseType>) {
        preconditionFailure("This method must be overriden by the subclass")
    }
    func requestCacheIfNotAlready(withCompletion completion: @escaping (CachedResponseType) -> ()) {
        start(existingTask: &requestCacheTask, implementation: { task in
            if shouldReuseCachedResponse, let response = controller.cachedResponseHolder.lastSuccessfulResponse {
                task.complete(withResult: response)
            } else {
                requestCache(withTask: task)
            }
        })
        requestCacheTask!.onComplete(handler: completion)
    }
    
    public private(set) var requestServerTask: AsyncTask<CachedResponseType>?
    open func requestServerAndUpdateCache(withTask task: AsyncTask<CachedResponseType>) {
        preconditionFailure("This method must be overriden by the subclass")
    }
    func requestServerIfNotAlready(withCompletion completion: @escaping (CachedResponseType) -> ()) {
        start(existingTask: &requestServerTask, implementation: { requestServerAndUpdateCache(withTask: $0) })
        requestServerTask!.onComplete(handler: completion)
        controller.remember(requestWithUnfinishedServerRequest: self, during: requestServerTask!)
    }
    
    open func postprocess(cachedResponse: CachedResponseType, task: AsyncTask<ResponseType>) {
        preconditionFailure("This method must be overriden by the subclass")
    }
}

//
//  RequestController.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 20/03/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public class ResponseHolder<ResponseType: AnyResponse> {
    public internal(set) var lastResponse: ResponseType?
    public internal(set) var lastSuccessfulResponse: ResponseType?
    public internal(set) var lastSuccessfulOrNotModifiedResponse: ResponseType?
    
    func remember(response: ResponseType) {
        lastResponse = response
        if response.unsafeResult.successful {
            lastSuccessfulResponse = response
        }
        if response.unsafeResult.successful || response.notModified {
            lastSuccessfulOrNotModifiedResponse = response
        }
    }
    
    func forgetResponse() {
        lastResponse = nil
        lastSuccessfulResponse = nil
        lastSuccessfulOrNotModifiedResponse = nil
    }
}

public class RequestController<ResponseType: AnyResponse, CachedResponseType: AnyResponse>: NSObject {
    var mutex = NSObject()
    public internal(set) var cachedResponseHolder = ResponseHolder<CachedResponseType>()
    public internal(set) var responseHolder = ResponseHolder<ResponseType>()
    public internal(set) var currentRequests: [Request<ResponseType, CachedResponseType>] = []
    public internal(set) var requestsWithUnfinishedServerRequests: [Request<ResponseType, CachedResponseType>] = []
    public var cacheRequestFabric: (() -> (Request<ResponseType, CachedResponseType>?))?
    var unhandledServerResponseHandlers: [(ResponseType) -> ()] = []
    
    func remember(currentRequest request: Request<ResponseType, CachedResponseType>, during task: AnyAsyncTask) {
        currentRequests.append(request)
        task.onCompleteIgnoringResult(afterCompletion: false) { 
            self.currentRequests.remove(at: self.currentRequests.index { $0 === request }!)
        }
    }
    
    func remember(requestWithUnfinishedServerRequest request: Request<ResponseType, CachedResponseType>, during task: AnyAsyncTask) {
        requestsWithUnfinishedServerRequests.append(request)
        task.onCompleteIgnoringResult(afterCompletion: false) { 
            self.requestsWithUnfinishedServerRequests.remove(at: self.requestsWithUnfinishedServerRequests.index { $0 === request }!)
        }
    }
    
    func unsafeMakeAndConfigureCacheRequest(reusingCachedResponse: Bool) -> Request<ResponseType, CachedResponseType>? {
        guard let request = cacheRequestFabric!() else { return nil }
        request.cachePolicy = .firstCacheIfFailedThenServer
        request.shouldAvoidCompletionOnTheSameEventLoop = false
        request.shouldSetNotModifiedIfNoSuccessfulServerResponse = false
        request.shouldReuseCachedResponse = reusingCachedResponse
        return request
    }
    
    func unhandledServerResponseDidHappen() {
        let handlers = unhandledServerResponseHandlers
        guard handlers.count > 0 && cacheRequestFabric != nil else { return }
        guard let request = self.unsafeMakeAndConfigureCacheRequest(reusingCachedResponse: true) else { return }
        request.perform { response in
            for handler in handlers {
                handler(response)
            }
        }
    }
    
    public func subscribeToUnhandledServerResponses(withHandler handler: @escaping (ResponseType) -> ()) {
        unhandledServerResponseHandlers.append(handler)
    }
    
    public func subscribeToExternalCacheNotifications(withAspects aspects: [String], handler: @escaping (ResponseType) -> ()) {
        CacheNotification.subscribe(aspects: aspects, holder: self) { [unowned self] notif in
            if let request = notif.request as? Request<ResponseType, CachedResponseType>, request.controller == self {
                return
            }
            guard let request = self.unsafeMakeAndConfigureCacheRequest(reusingCachedResponse: false) else { return }
            request.perform(withCompletion: handler)
        }
    }
}

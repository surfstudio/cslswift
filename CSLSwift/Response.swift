//
//  Response.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 27/02/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public protocol AnyResponse {
    var unsafeResult: AnyResult { get }
    var notModified: Bool { get set }
}

open class Response<ResultType>: AnyResponse {
    public var result: Result<ResultType>
    public var unsafeResult: AnyResult {
        return result
    }
    public var notModified: Bool = false
    public init(result: Result<ResultType>) {
        self.result = result
    }
}

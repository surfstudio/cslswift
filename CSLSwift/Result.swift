//
//  Result.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 13/03/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public protocol AnyResult {
    var successful: Bool { get }
    var error: Error? { get }
}

public enum Result<Value>: AnyResult {
    case success(Value)
    case failure(Error)
    
    public var successful: Bool {
        switch self {
        case .success(_): return true
        case .failure(_): return false
        }
    }
    
    public var error: Error? {
        switch self {
        case .success(_): return nil
        case let .failure(error): return error
        }
    }

    public var value: Value? {
        switch self {
        case let .success(value): return value
        case .failure(_): return nil
        }
    }
}

public extension Result {
    public init(block: () throws -> Value) {
        do {
            self = .success(try block())
        } catch {
            self = .failure(error)
        }
    }
}

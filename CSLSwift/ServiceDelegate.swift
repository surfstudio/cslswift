//
//  ServiceDelegate.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 27/02/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public protocol ServiceDelegate {
    func requestWasCreated(request: AnyRequest)
    func requestWillStart(request: AnyRequest)
    func requestDidFinish(request: AnyRequest)
    func requestDidReceiveUnhandledResponse(request: AnyRequest, cachedResponse: AnyResponse)
}

public extension ServiceDelegate {
    func requestWasCreated(request: AnyRequest) {}
    func requestWillStart(request: AnyRequest) {}
    func requestDidFinish(request: AnyRequest) {}
    func requestDidReceiveUnhandledResponse(request: AnyRequest, cachedResponse: AnyResponse) {}
}

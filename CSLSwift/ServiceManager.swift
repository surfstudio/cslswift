//
//  ServiceManager.swift
//  CSLSwift
//
//  Created by Dmitrii Trofimov on 27/02/2017.
//  Copyright © 2017 Surf Studio. All rights reserved.
//

import Foundation

public class ServiceManager {
    public static let shared = ServiceManager()
    public var delegate: ServiceDelegate?
}
